package com.jaa.driver.common.swipe_button;

public interface OnActiveListener {
    void onActive();
}
