package com.jaa.driver.ui.activity.forgot_password;

import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.ForgotResponse;

public interface ForgotIView extends MvpView {

    void onSuccess(ForgotResponse forgotResponse);
    void onError(Throwable e);
}
