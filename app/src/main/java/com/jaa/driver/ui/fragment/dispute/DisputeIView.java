package com.jaa.driver.ui.fragment.dispute;

import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.DisputeResponse;

import java.util.List;

public interface DisputeIView extends MvpView {

    void onSuccessDispute(List<DisputeResponse> responseList);

    void onSuccess(Object object);

    void onError(Throwable e);
}
