package com.jaa.driver.ui.activity.instant_ride;

import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.EstimateFare;
import com.jaa.driver.data.network.model.TripResponse;

public interface InstantRideIView extends MvpView {

    void onSuccess(EstimateFare estimateFare);

    void onSuccess(TripResponse response);

    void onError(Throwable e);

}
