package com.jaa.driver.ui.activity.main;

import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.SettingsResponse;
import com.jaa.driver.data.network.model.TripResponse;
import com.jaa.driver.data.network.model.UserResponse;

public interface MainIView extends MvpView {
    void onSuccess(UserResponse user);

    void onError(Throwable e);

    void onSuccessLogout(Object object);

    void onSuccess(TripResponse tripResponse);

    void onSuccess(SettingsResponse response);

    void onSettingError(Throwable e);

    void onSuccessProviderAvailable(Object object);

    void onSuccessFCM(Object object);

    void onSuccessLocationUpdate(TripResponse tripResponse);

}
