package com.jaa.driver.ui.activity.earnings;


import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.EarningsList;

public interface EarningsIView extends MvpView {

    void onSuccess(EarningsList earningsLists);

    void onError(Throwable e);
}
