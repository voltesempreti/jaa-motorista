package com.jaa.driver.ui.activity.help;


import com.jaa.driver.base.MvpPresenter;

public interface HelpIPresenter<V extends HelpIView> extends MvpPresenter<V> {

    void getHelp();
}
