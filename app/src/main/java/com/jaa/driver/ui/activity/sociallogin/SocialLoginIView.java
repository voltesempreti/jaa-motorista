package com.jaa.driver.ui.activity.sociallogin;

import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.Token;

public interface SocialLoginIView extends MvpView {

    void onSuccess(Token token);
    void onError(Throwable e);
}
