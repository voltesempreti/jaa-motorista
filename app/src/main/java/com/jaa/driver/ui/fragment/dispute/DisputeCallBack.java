package com.jaa.driver.ui.fragment.dispute;

public interface DisputeCallBack {
    void onDisputeCreated();
}
