package com.jaa.driver.ui.activity.invite_friend;

import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.UserResponse;

public interface InviteFriendIView extends MvpView {

    void onSuccess(UserResponse response);
    void onError(Throwable e);

}
