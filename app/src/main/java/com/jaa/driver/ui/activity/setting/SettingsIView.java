package com.jaa.driver.ui.activity.setting;

import com.jaa.driver.base.MvpView;

public interface SettingsIView extends MvpView {

    void onSuccess(Object o);

    void onError(Throwable e);

}
