package com.jaa.driver.ui.fragment.upcoming;

import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.HistoryList;

import java.util.List;

public interface UpcomingTripIView extends MvpView {

    void onSuccess(List<HistoryList> historyList);
    void onError(Throwable e);
}
