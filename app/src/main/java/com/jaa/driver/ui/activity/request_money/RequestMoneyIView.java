package com.jaa.driver.ui.activity.request_money;

import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.RequestDataResponse;

public interface RequestMoneyIView extends MvpView {

    void onSuccess(RequestDataResponse response);
    void onSuccess(Object response);
    void onError(Throwable e);

}
