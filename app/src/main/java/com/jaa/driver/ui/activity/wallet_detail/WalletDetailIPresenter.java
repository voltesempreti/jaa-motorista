package com.jaa.driver.ui.activity.wallet_detail;

import com.jaa.driver.base.MvpPresenter;
import com.jaa.driver.data.network.model.Transaction;

import java.util.ArrayList;

public interface WalletDetailIPresenter<V extends WalletDetailIView> extends MvpPresenter<V> {
    void setAdapter(ArrayList<Transaction> myList);
}
