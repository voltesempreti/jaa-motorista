package com.jaa.driver.ui.activity.regsiter;

import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.SettingsResponse;
import com.jaa.driver.data.network.model.User;

public interface RegisterIView extends MvpView {

    void onSuccess(User user);

    void onSuccess(Object verifyEmail);

    void onSuccess(SettingsResponse response);

    void onError(Throwable e);

    void onSuccessPhoneNumber(Object object);

    void onVerifyPhoneNumberError(Throwable e);

    void onVerifyEmailError(Throwable e);

}
