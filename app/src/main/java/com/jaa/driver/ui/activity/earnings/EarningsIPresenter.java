package com.jaa.driver.ui.activity.earnings;


import com.jaa.driver.base.MvpPresenter;

public interface EarningsIPresenter<V extends EarningsIView> extends MvpPresenter<V> {

    void getEarnings();
}
