package com.jaa.driver.ui.activity.wallet_detail;

import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.Transaction;

import java.util.ArrayList;

public interface WalletDetailIView extends MvpView {
    void setAdapter(ArrayList<Transaction> myList);
}
