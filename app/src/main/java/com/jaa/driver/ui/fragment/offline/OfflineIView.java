package com.jaa.driver.ui.fragment.offline;

import com.jaa.driver.base.MvpView;

public interface OfflineIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}
