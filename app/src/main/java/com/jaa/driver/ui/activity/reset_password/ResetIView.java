package com.jaa.driver.ui.activity.reset_password;

import com.jaa.driver.base.MvpView;

public interface ResetIView extends MvpView{

    void onSuccess(Object object);
    void onError(Throwable e);
}
