package com.jaa.driver.ui.fragment.past;


import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.HistoryList;

import java.util.List;

public interface PastTripIView extends MvpView {

    void onSuccess(List<HistoryList> historyList);
    void onError(Throwable e);
}
