package com.jaa.driver.ui.bottomsheetdialog.rating;

import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.Rating;

public interface RatingDialogIView extends MvpView {

    void onSuccess(Rating rating);
    void onError(Throwable e);
}
