package com.jaa.driver.ui.activity.welcome;

import com.jaa.driver.base.MvpPresenter;

public interface WelcomeIPresenter<V extends WelcomeIView> extends MvpPresenter<V> {
}
