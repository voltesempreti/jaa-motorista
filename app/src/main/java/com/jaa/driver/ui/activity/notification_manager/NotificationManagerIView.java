package com.jaa.driver.ui.activity.notification_manager;

import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.NotificationManager;

import java.util.List;

public interface NotificationManagerIView extends MvpView {

    void onSuccess(List<NotificationManager> managers);

    void onError(Throwable e);

}