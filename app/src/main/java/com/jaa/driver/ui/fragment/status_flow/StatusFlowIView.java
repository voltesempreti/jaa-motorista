package com.jaa.driver.ui.fragment.status_flow;

import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.TimerResponse;

public interface StatusFlowIView extends MvpView {

    void onSuccess(Object object);

    void onWaitingTimeSuccess(TimerResponse object);

    void onError(Throwable e);
}
