package com.jaa.driver.ui.bottomsheetdialog.invoice_flow;

import com.jaa.driver.base.MvpView;

public interface InvoiceDialogIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}
