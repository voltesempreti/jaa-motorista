package com.jaa.driver.ui.fragment.past;


import com.jaa.driver.base.MvpPresenter;

public interface PastTripIPresenter<V extends PastTripIView> extends MvpPresenter<V> {

    void getHistory();

}
