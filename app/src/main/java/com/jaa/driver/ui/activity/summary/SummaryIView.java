package com.jaa.driver.ui.activity.summary;


import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.Summary;

public interface SummaryIView extends MvpView {

    void onSuccess(Summary object);

    void onError(Throwable e);
}
