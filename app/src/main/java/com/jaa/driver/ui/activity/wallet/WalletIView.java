package com.jaa.driver.ui.activity.wallet;

import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.WalletMoneyAddedResponse;
import com.jaa.driver.data.network.model.WalletResponse;

public interface WalletIView extends MvpView {

    void onSuccess(WalletResponse response);

    void onSuccess(WalletMoneyAddedResponse response);

    void onError(Throwable e);
}
