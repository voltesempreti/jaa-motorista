package com.jaa.driver.ui.activity.password;

import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.ForgotResponse;
import com.jaa.driver.data.network.model.User;

public interface PasswordIView extends MvpView {

    void onSuccess(ForgotResponse forgotResponse);

    void onSuccess(User object);

    void onError(Throwable e);
}
