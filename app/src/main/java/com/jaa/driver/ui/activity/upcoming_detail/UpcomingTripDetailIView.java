package com.jaa.driver.ui.activity.upcoming_detail;


import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.HistoryDetail;

public interface UpcomingTripDetailIView extends MvpView {

    void onSuccess(HistoryDetail historyDetail);
    void onError(Throwable e);
}
