package com.jaa.driver.ui.activity.past_detail;


import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.HistoryDetail;

public interface PastTripDetailIView extends MvpView {

    void onSuccess(HistoryDetail historyDetail);
    void onError(Throwable e);
}
