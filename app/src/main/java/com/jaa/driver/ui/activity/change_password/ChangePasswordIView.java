package com.jaa.driver.ui.activity.change_password;

import com.jaa.driver.base.MvpView;

public interface ChangePasswordIView extends MvpView {


    void onSuccess(Object object);
    void onError(Throwable e);
}
