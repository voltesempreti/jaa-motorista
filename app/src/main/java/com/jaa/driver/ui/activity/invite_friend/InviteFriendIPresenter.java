package com.jaa.driver.ui.activity.invite_friend;

import com.jaa.driver.base.MvpPresenter;

public interface InviteFriendIPresenter<V extends InviteFriendIView> extends MvpPresenter<V> {
    void profile();
}
