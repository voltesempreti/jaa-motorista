package com.jaa.driver.ui.activity.help;

import com.jaa.driver.base.MvpView;
import com.jaa.driver.data.network.model.Help;

public interface HelpIView extends MvpView {

    void onSuccess(Help object);

    void onError(Throwable e);
}
